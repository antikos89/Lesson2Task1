﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Employe[] employes=new Employe[5];

           
            employes[0] = new FullTimeStuff("Kostya", 25000);
            employes[1] = new HourlyStuff("Vanya",2500);
            employes[2] = new HourlyStuff("Igor", 1500);
            employes[3] = new FullTimeStuff("Kolya", 35000);
            employes[4] = new HourlyStuff("Andrey", 978);


            Array.Sort(employes);
            foreach (Employe emp in employes)
            {
                Console.WriteLine("У работника {0} зарплата в месяц {1} руб. ", emp.Name, emp.GetSalary());
            }
               
            Console.ReadLine();
        }
    }
}
