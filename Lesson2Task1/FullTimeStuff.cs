﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson2Task1;

namespace Lesson2Task1
{
    class FullTimeStuff:Employe
    {
        /// <summary>
        /// Зарплпта работника
        /// </summary>
        double Salary;

        /// <summary>
        /// Штатный сотрудник с фиксированной ставкой
        /// </summary>
        /// <param name="_name">Имя работника</param>
        /// <param name="_salary">Фиксироанная зарплата за месяц</param>
        public FullTimeStuff(string _name,double _salary):base(_name)
        {
            Salary = _salary;
        }
       
        /// <summary>
        /// Возвращает зарплату работника
        /// </summary>
        /// <returns></returns>
        public override double GetSalary()
        {
            return Salary;
        }
    }
}
