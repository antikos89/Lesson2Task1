﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2Task1
{
    abstract class Employe:IComparable
    {
        public string Name { get; set; }

        public Employe(string _name)
        {
            Name = _name;
        }

        /// <summary>
        /// Компаратор для сортировки массива сотрудников по имени
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Object obj)
        {
            Employe emp = obj as Employe;
            if (emp != null)
            {
                return this.Name.CompareTo(emp.Name);
            }
            else
            {
                throw new Exception("Невозможно сравнить два объекта");
            }
        }

        /// <summary>
        /// Абстракный метод расчета зарплаты
        /// </summary>
        /// <returns></returns>
        public abstract double GetSalary();

    }
}
