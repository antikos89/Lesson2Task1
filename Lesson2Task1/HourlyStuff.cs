﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2Task1
{
     class HourlyStuff:Employe
    {
        double HourlyPay;
        double Salary;

        /// <summary>
        /// Почасовой работник
        /// </summary>
        /// <param name="_name">Имя работника</param>
        /// <param name="_hourlyPay">Почасовая оплата</param>
        public HourlyStuff(string _name,double _hourlyPay):base(_name)
        {
            HourlyPay=_hourlyPay;
        }

        /// <summary>
        /// Возвращает зарплату работника
        /// </summary>
        /// <returns></returns>
        public override double GetSalary()
        {
            Salary = 20.8 * 8 * HourlyPay;
            return Salary;
        }

    }
}
